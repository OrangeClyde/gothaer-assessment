# Preconditions & thoughts

<!-- TOC -->

1. [Data resolution](#data-resolution)
2. [Styling](#styling)
3. [Products](#products)
4. [Pricing](#pricing)
5. [Testing](#testing)
6. [Additions](#additions)
7. [To wrap it up](#to-wrap-it-up)

<!-- /TOC -->

## Data resolution

To provide the data I've implemented a **mock API**. It provides all the data in the way it's been presented in the given JSON files, except for one exception when it comes to the user data. \
When **fetching a customer**, rather than returning the full list of customers, the API just returns the data related to the currently signed in user (to keep up the appearance of GDPR conformity 😉).

As the remaining data is generic, it's just **fetched once and then reused** across the application, keeping the amount of requests low. Still a refresh timeout for this could be a meaningful addition to keep the data updated even if e.g. the user keeps the tab open for while.

Finally, I've skipped adding a central **state management** for the scope of this assessment, however looking at the given scenario and data structure as well as the necessity to combine data in the frontend, state management would be an option to consider.

## Styling

For this assessment, I took the freedom to assume a set of rather modern browsers, using flexbox for styling - surely this depends on the targeted group of browsers.

For the coloring I loosely took bearings from the Gothaer colors, apart from that I went for a basic and clean style.

## Products

As the task just outlined to show the available insurances, the **stickers** have been left out for this implementation. Still the service retrieves and handles all products, so adding stickers wouldn't be much effort.

## Pricing

The prices are presented as given in the JSON files. However, the question came to my mind if those are just made up for the sake of creating this scenario or actually make sense in some way. As this could also be a question for actual customers, clarifying the meaning of the price would also be an enhancement for the UI.

Apart from that I'm personally curious if there's a sense behind them!

## Testing

Starting off this assessment, I went for the implementation first while adding the unit tests afterwards, prioritizing the completion of the application in the given timeframe.

## Additions

Even though e.g. the registration and the membership list weren't part of the given assessment, I added those parts to showcase possible additions.

Finally, in a real-world scenario, there'd surely be some polishing left, looking at e.g the user guidance in forms.

Overall, I really enjoyed working on this assessment and my feeling is that the application suits the scope it - therefore I hope it also meets your expectations.

## To wrap it up

Thanks a lot for your time, I'm looking forward to hearing from you!
