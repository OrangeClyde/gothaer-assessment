# Project structure & basic setup

<!-- TOC -->

1. [Module structure](#module-structure)
  1. [Authentication module](#authentication-module)
  2. [Mock module](#mock-module)
  3. [Profile module](#profile-module)
  4. [Shared module](#shared-module)
2. [Code-formatting with Prettier](#code-formatting-with-prettier)

<!-- /TOC -->

## Module structure

### Authentication module

The authentication module exposes all **authentication related business logic**.

Contains authentication related route definitions for login and registration, route guards and the authentication service. Also it's central to determine the routing on load based on the user's current authentication state.

### Mock module

To run the app without a working backend, a **mock environment** has been added. The mock module itself is mainly useful for testing purposes. When serving using the mock environment, the interceptor is injected using the environment file.

In case you want to simulate a slow API, head over to the [MockRequestInterceptor](../src/app/mock/interceptors/mock-request.interceptor.ts) and tweak the static delay on top of the class.

### Profile module

This module accumulates all **profile routes and its child routes** as well as the related components and services.

It's **lazy-loaded** as loading the profile chunk is not necessary for unauthenticated users.

### Shared module

This module contains common imports as well as **shared services and components**, that are relevant to other components across the application.

## Code-formatting with Prettier

To maintain a clean and consistent code style in an automated fashion, Prettier is a helpful utility, describing itself as an opinionated code formatter.

It exposes a [couple of settings](https://prettier.io/docs/en/options.html), however, as it is opinionated, a lot of the code style is actually pre-defined. Accordingly, the TSLint configuration got an additional extension to resolve Prettier conflicts.