# Gothaer assessment

## Getting started

```
npm install
npm run start
```

## Supportive documentation

Please check the following documents for more information on the assessment and the thoughts behind it.

### &rarr; [Project setup & structure](./docs/Structure.md)

### &rarr; [Preconditions & thoughts](./docs/Preconditions.md)

## Central commands

```shell
# Serve locally mocked
npm run start

# Run unit tests
npm run test

# Build
npm run build
```

> This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.21.
