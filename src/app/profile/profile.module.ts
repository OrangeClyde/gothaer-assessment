import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { InsurancePriceComponent } from './components/insurance-price/insurance-price.component';
import { ProfileDetailsComponent } from './components/profile-details/profile-details.component';
import { ProfileInsurancesComponent } from './components/profile-insurances/profile-insurances.component';
import { ProfileMembershipsComponent } from './components/profile-memberships/profile-memberships.component';
import { ProfileNavigationComponent } from './components/profile-navigation/profile-navigation.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileInsuranceListComponent } from './components/profile-insurance-list/profile-insurance-list.component';

@NgModule({
  imports: [SharedModule, ProfileRoutingModule],
  declarations: [
    ProfileComponent,
    ProfileNavigationComponent,
    ProfileInsurancesComponent,
    ProfileMembershipsComponent,
    InsurancePriceComponent,
    ProfileDetailsComponent,
    ProfileInsuranceListComponent
  ]
})
export class ProfileModule {}
