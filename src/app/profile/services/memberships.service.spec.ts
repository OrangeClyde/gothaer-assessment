import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthenticationModule } from '../../authentication/authentication.module';
import { MockModule } from '../../mock/mock.module';
import { MembershipType } from '../../shared/models/MembershipType';
import { MembershipsService } from './memberships.service';

describe('MembershipsService', () => {
  let service: MembershipsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule.withRoutes([]),
        AuthenticationModule.forRoot(),
        MockModule.forRoot()
      ]
    });
    service = TestBed.get(MembershipsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get memberships', async () => {
    const membershipMap: Map<string, MembershipType> = await service.getMemberships().toPromise();

    expect(membershipMap).toBeDefined();
    expect(membershipMap.size).toBeGreaterThan(0);
  });
});
