import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthenticationModule } from '../../authentication/authentication.module';
import { MockModule } from '../../mock/mock.module';
import { Insurance } from '../../shared/models/Insurance';
import { Product } from '../../shared/models/Product';
import { Sticker } from '../../shared/models/Sticker';
import { ProductsService } from './products.service';

describe('ProductsService', () => {
  let service: ProductsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule.withRoutes([]),
        AuthenticationModule.forRoot(),
        MockModule.forRoot()
      ]
    });
    service = TestBed.get(ProductsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all products', async () => {
    const products: Product[] = await service.getProducts().toPromise();

    expect(products.some(p => p instanceof Insurance)).toBeTruthy();
    expect(products.some(p => p instanceof Sticker)).toBeTruthy();
  });

  it('should get all products', async () => {
    const insurances: Insurance[] = await service.getInsurances().toPromise();

    expect(insurances.some(p => p instanceof Insurance)).toBeTruthy();
    expect(insurances.some(p => p instanceof Sticker)).toBeFalsy();
  });
});
