import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MembershipType } from 'src/app/shared/models/MembershipType';

@Injectable({
  providedIn: 'root'
})
export class MembershipsService {
  private memberships$: Observable<Map<string, MembershipType>>;

  constructor(private httpClient: HttpClient) {}

  public getMemberships(): Observable<Map<string, MembershipType>> {
    if (!this.memberships$) {
      this.memberships$ = this.httpClient
        .get<{ membership: [{ [membershipName: string]: { level: number } }] }>('/api/memberships')
        .pipe(map(this.mapMemberships.bind(this)), shareReplay<Map<string, MembershipType>>(1));
    }

    return this.memberships$;
  }

  private mapMemberships({ membership }): Map<string, MembershipType> {
    const membershipMap: Map<string, MembershipType> = new Map<string, MembershipType>();
    Object.keys(membership[0]).map(name =>
      membershipMap.set(name, new MembershipType(name, membership[0][name].level))
    );

    return membershipMap;
  }
}
