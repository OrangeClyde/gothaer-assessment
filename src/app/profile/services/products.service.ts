import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { Product, ProductType } from '../../shared/models/Product';
import { Sticker } from '../../shared/models/Sticker';
import { Insurance } from './../../shared/models/Insurance';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private products$: Observable<Product[]>;

  constructor(private httpClient: HttpClient) {}

  public getProducts(): Observable<Product[]> {
    if (!this.products$) {
      this.products$ = this.httpClient
        .get<Product[]>('/api/products')
        .pipe(map(this.mapProducts.bind(this)), publishReplay<Product[]>(1), refCount());
    }

    return this.products$;
  }

  public getInsurances(): Observable<Insurance[]> {
    return this.getProducts().pipe(map(ps => ps.filter(p => p instanceof Insurance))) as Observable<Insurance[]>;
  }

  private mapProducts(productObjs: any): Product[] {
    const products = Object.keys(productObjs)
      .map((n: string) => {
        const product = productObjs[n];

        switch (product.type) {
          case ProductType.Insurance:
            return Insurance.fromObject(n, product);
          case ProductType.Sticker:
            return Sticker.fromObject(n, product);
          default:
            console.warn('Unsupported product type "%s" detected.', product.type);
            return null;
        }
      })
      .filter(p => !!p);

    return products;
  }
}
