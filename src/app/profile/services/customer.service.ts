import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { Message } from 'src/app/shared/models/Message';

import { AuthenticationService } from '../../authentication/services/authentication.service';
import { Customer } from '../../shared/models/Customer';
import { MessageService } from './../../shared/services/message.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  public currentCustomer$: BehaviorSubject<Customer> = new BehaviorSubject<Customer>(null);

  constructor(
    private httpClient: HttpClient,
    private authenticationService: AuthenticationService,
    private messageService: MessageService
  ) {
    this.authenticationService.authenticationState$.subscribe(this.onAuthenticationStateChanged.bind(this));
  }

  private getCustomer() {
    return this.httpClient
      .get<Customer>(`/api/customers/${this.authenticationService.customerName}`)
      .pipe(map<any, Customer>(this.mapCurrentCustomer.bind(this)));
  }

  private mapCurrentCustomer(customerObj: any) {
    const customer = Customer.fromObject(customerObj);
    this.currentCustomer$.next(customer);
    return customer;
  }

  private onAuthenticationStateChanged(authenticated: boolean) {
    if (authenticated) {
      this.getCustomer()
        .pipe(first())
        .subscribe({
          next: c => this.currentCustomer$.next(c),
          error: this.handleError.bind(this)
        });
      return;
    }

    this.currentCustomer$.next(null);
  }

  private handleError() {
    this.messageService.queueMessage(
      new Message(`You have been signed out automatically. Please try to sign in again.`)
    );
    this.authenticationService.signOut();
  }
}
