import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { AuthenticationModule } from '../../authentication/authentication.module';
import { AuthenticationService } from '../../authentication/services/authentication.service';
import { MockModule } from '../../mock/mock.module';
import { Customer } from '../../shared/models/Customer';
import { MessageService } from './../../shared/services/message.service';
import { CustomerService } from './customer.service';

describe('CustomerService', () => {
  let service: CustomerService;
  let authenticationService: AuthenticationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule.withRoutes([]),
        AuthenticationModule.forRoot(),
        MockModule.forRoot()
      ],
      providers: [MessageService]
    });

    service = TestBed.get(CustomerService);
    authenticationService = TestBed.get(AuthenticationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should react to a successful authentication', () => {
    const stubCustomer = new Customer('foo', 'basic', 29, []);

    spyOn(service as any, 'getCustomer').and.returnValue(of(stubCustomer));

    authenticationService.authenticationState$.next(true);

    expect(service['getCustomer']).toHaveBeenCalled();
    expect(service.currentCustomer$.value).toBe(stubCustomer);
  });

  it('should react to a sign out', () => {
    const stubCustomer = new Customer('foo', 'basic', 29, []);
    service.currentCustomer$.next(stubCustomer);

    authenticationService.authenticationState$.next(false);

    expect(service.currentCustomer$.value).toBe(null);
  });

  it('should map the customer', async () => {
    const customer = service['mapCurrentCustomer']({
      name: 'foo',
      membership_type: 'basic',
      age: '29',
      selected_insurances: []
    });

    expect(customer instanceof Customer).toBeTruthy();
    expect(customer.name).toEqual('foo');
    expect(customer.membership).toEqual('basic');
    expect(customer.age).toEqual(29);
  });
});
