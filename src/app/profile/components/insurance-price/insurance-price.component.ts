import { Component, Input } from '@angular/core';

import { Price } from '../../../shared/models/Price';

@Component({
  selector: 'app-insurance-price',
  templateUrl: './insurance-price.component.html',
  styleUrls: ['./insurance-price.component.scss']
})
export class InsurancePriceComponent {
  @Input()
  public price: Price;
}
