import { Component, Input } from '@angular/core';

import { Customer } from '../../../shared/models/Customer';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html',
  styleUrls: ['./profile-details.component.scss']
})
export class ProfileDetailsComponent {
  @Input()
  public customer: Customer;
}
