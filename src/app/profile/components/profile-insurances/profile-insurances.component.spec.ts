import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { Customer } from 'src/app/shared/models/Customer';
import { Insurance } from 'src/app/shared/models/Insurance';
import { Price } from 'src/app/shared/models/Price';

import { AuthenticationModule } from '../../../authentication/authentication.module';
import { MockModule } from '../../../mock/mock.module';
import { MembershipType } from '../../../shared/models/MembershipType';
import { ProfileModule } from '../../profile.module';
import { ProfileInsurancesComponent } from './profile-insurances.component';

describe('ProfileInsurancesComponent', () => {
  let component: ProfileInsurancesComponent;
  let fixture: ComponentFixture<ProfileInsurancesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ProfileModule,
        RouterTestingModule.withRoutes([]),
        HttpClientModule,
        AuthenticationModule.forRoot(),
        MockModule.forRoot()
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileInsurancesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should map selected insurances based on a customer', () => {
    component.insurances = [
      new Insurance('available_basic', 'basic', [new Price('€', 1, 18, 100)]),
      new Insurance('available_plus', 'plus', [new Price('€', 1, 0, 18)]),
      new Insurance('!premium', 'premium', [new Price('€', 1, 0, 100)]),
      new Insurance('!age', 'basic', [new Price('€', 1, 90, 100)]),
      new Insurance('selected_premium', 'premium', [new Price('€', 1, 0, 100)]),
      new Insurance('selected_age', 'basic', [new Price('€', 1, 90, 100)])
    ];

    component.memberships = new Map<string, MembershipType>();
    component.memberships.set('basic', new MembershipType('basic', 1));
    component.memberships.set('plus', new MembershipType('plus', 2));
    component.memberships.set('premium', new MembershipType('premium', 3));

    component['mapSelectedInsurances'](new Customer('foo', 'plus', 18, ['selected_premium', 'selected_age']));

    expect(component.selectedInsurances[0].name).toEqual('selected_premium');
    expect(component.selectedInsurances[1].name).toEqual('selected_age');

    expect(component.availableInsurances[0].name).toEqual('available_basic');
    expect(component.availableInsurances[1].name).toEqual('available_plus');
  });
});
