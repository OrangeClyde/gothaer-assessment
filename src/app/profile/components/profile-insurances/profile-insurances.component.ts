import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { MembershipType } from 'src/app/shared/models/MembershipType';

import { Customer } from '../../../shared/models/Customer';
import { CustomerService } from '../../services/customer.service';
import { MembershipsService } from '../../services/memberships.service';
import { ProductsService } from '../../services/products.service';
import { Insurance } from './../../../shared/models/Insurance';
import { MessageService } from './../../../shared/services/message.service';

@Component({
  selector: 'app-profile-insurances',
  templateUrl: './profile-insurances.component.html',
  styleUrls: ['./profile-insurances.component.scss']
})
export class ProfileInsurancesComponent {
  public customer: Customer;
  public insurances: Insurance[];
  public memberships: Map<string, MembershipType>;
  public selectedInsurances: Insurance[];
  public availableInsurances: Insurance[];

  constructor(
    private customerService: CustomerService,
    private productsService: ProductsService,
    private membershipsService: MembershipsService,
    private messageService: MessageService
  ) {
    this.fetch();
  }

  private fetch() {
    forkJoin(this.fetchMemberships(), this.fetchInsurances()).subscribe({
      next: this.onFetchSuccess.bind(this),
      error: this.handleError.bind(this)
    });
  }

  private fetchMemberships(): Observable<Map<string, MembershipType>> {
    return this.membershipsService.getMemberships().pipe(first());
  }

  private fetchInsurances(): Observable<Insurance[]> {
    return this.productsService.getInsurances().pipe(first());
  }

  private onFetchSuccess(result: [Map<string, MembershipType>, Insurance[]]) {
    this.memberships = result[0];
    this.insurances = result[1];
    this.customerService.currentCustomer$
      .pipe<Customer>(filter(c => !!c))
      .subscribe(this.mapSelectedInsurances.bind(this));
  }

  private mapSelectedInsurances(customer: Customer) {
    this.customer = customer;

    const customerLevel = this.memberships.get(customer.membership).level;
    this.selectedInsurances = [];
    this.availableInsurances = this.insurances.filter(i => {
      if (customer.selectedInsurances.indexOf(i.name) >= 0) {
        this.selectedInsurances.push(i);
        return false;
      }

      const insuranceLevel = this.memberships.get(i.availability).level;
      if (insuranceLevel > customerLevel || !i.getPriceForAge(customer.age)) {
        return false;
      }

      return true;
    });
  }

  private handleError(err: HttpErrorResponse) {
    this.messageService.queueMessage(err ? err.error : 'Something went wrong. Please try to reload the page.');
  }
}
