import { Component } from '@angular/core';

import { AuthenticationService } from '../../../authentication/services/authentication.service';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  constructor(private authenticationService: AuthenticationService, public customerService: CustomerService) {}

  signOut() {
    this.authenticationService.signOut();
  }
}
