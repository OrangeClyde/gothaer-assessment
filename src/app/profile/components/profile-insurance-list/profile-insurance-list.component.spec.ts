import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Customer } from 'src/app/shared/models/Customer';
import { Insurance } from 'src/app/shared/models/Insurance';
import { Price } from 'src/app/shared/models/Price';

import { LoaderComponent } from '../../../shared/components/loader/loader.component';
import { InsurancePriceComponent } from '../insurance-price/insurance-price.component';
import { ProfileInsuranceListComponent } from './profile-insurance-list.component';

describe('ProfileInsuranceListComponent', () => {
  let component: ProfileInsuranceListComponent;
  let fixture: ComponentFixture<ProfileInsuranceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, BrowserAnimationsModule],
      declarations: [ProfileInsuranceListComponent, InsurancePriceComponent, LoaderComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileInsuranceListComponent);
    component = fixture.componentInstance;
    component.customer = new Customer('foo', 'basic', 18, []);
    component.insurances = [new Insurance('foo_bar', 'basic', [new Price('€', 1.5, 0, 100, '0-100')])];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should track insurance entries by name', () => {
    expect(component.trackName(0, component.insurances[0])).toEqual(component.insurances[0].name);
  });
});
