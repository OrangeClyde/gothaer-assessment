import { Attribute, Component, Input } from '@angular/core';

import { Customer } from '../../../shared/models/Customer';
import { Insurance } from '../../../shared/models/Insurance';

@Component({
  selector: 'app-profile-insurance-list',
  templateUrl: './profile-insurance-list.component.html',
  styleUrls: ['./profile-insurance-list.component.scss']
})
export class ProfileInsuranceListComponent {
  @Input()
  public insurances: Insurance[];

  @Input()
  public customer: Customer;

  constructor(@Attribute('emptyStateText') public emptyStateText: string) {}

  public trackName(index: number, insurance: Insurance) {
    return insurance.name;
  }
}
