import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { first } from 'rxjs/operators';
import { MembershipType } from 'src/app/shared/models/MembershipType';

import { CustomerService } from '../../services/customer.service';
import { MembershipsService } from '../../services/memberships.service';
import { MessageService } from './../../../shared/services/message.service';

@Component({
  selector: 'app-profile-memberships',
  templateUrl: './profile-memberships.component.html',
  styleUrls: ['./profile-memberships.component.scss']
})
export class ProfileMembershipsComponent {
  public memberships: MembershipType[];

  constructor(
    public customerService: CustomerService,
    private membershipService: MembershipsService,
    private messageService: MessageService
  ) {
    this.fetchMemberships();
  }

  private fetchMemberships() {
    return this.membershipService
      .getMemberships()
      .pipe(first())
      .subscribe({
        next: this.assignMemberships.bind(this),
        error: this.handleError.bind(this)
      });
  }

  private assignMemberships(memberships: Map<string, MembershipType>) {
    this.memberships = [...memberships.entries()].map(entry => entry[1]).sort((m1, m2) => m1.level - m2.level);
  }

  private handleError(err: HttpErrorResponse) {
    this.messageService.queueMessage(err ? err.error : 'Something went wrong. Please try to reload the page.');
  }
}
