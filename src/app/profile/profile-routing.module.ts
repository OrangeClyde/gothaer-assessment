import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { ProfileInsurancesComponent } from './components/profile-insurances/profile-insurances.component';
import { ProfileMembershipsComponent } from './components/profile-memberships/profile-memberships.component';
import { ProfileComponent } from './components/profile/profile.component';

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'profile'
  },
  {
    path: 'profile',
    component: ProfileComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'insurances' },
      { path: 'insurances', component: ProfileInsurancesComponent },
      { path: 'memberships', component: ProfileMembershipsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {}
