import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { LoaderComponent } from './components/loader/loader.component';
import { MessageComponent } from './components/message/message.component';

@NgModule({
  declarations: [LoaderComponent, MessageComponent],
  imports: [CommonModule, FormsModule],
  exports: [CommonModule, FormsModule, LoaderComponent, MessageComponent]
})
export class SharedModule {}
