import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { flyInOutTrigger } from '../../animations/fly-in-out.animation';
import { Message, MessageType } from '../../models/Message';
import { MessageService } from './../../services/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  animations: [flyInOutTrigger]
})
export class MessageComponent {
  public MessageType = MessageType;
  public currentMessage$: BehaviorSubject<Message>;

  constructor(messageService: MessageService) {
    this.currentMessage$ = messageService.currentMessage$;
  }
}
