import { TestBed } from '@angular/core/testing';

import { Message } from '../models/Message';
import { MessageService } from './message.service';

describe('MessageService', () => {
  let service: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(MessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should queue a message and start the loop', () => {
    service.queueMessage(new Message('test'));

    expect(service.currentMessage$.value.text).toEqual('test');
  });

  it('should not start the loop again in case a message is already queued', () => {
    service.queueMessage(new Message('test'));

    expect(service.currentMessage$.value.text).toEqual('test');

    service.queueMessage(new Message('test2'));

    expect(service.currentMessage$.value.text).toEqual('test');
  });

  it('should stop the loop if queue is empty', () => {
    service.queueMessage(new Message('test'));
    service['next']();

    expect(service['looping']).toBeFalsy();
  });
});
