import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Message } from '../models/Message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  public currentMessage$: BehaviorSubject<Message> = new BehaviorSubject<Message>(null);
  private messageQueue: Message[] = [];
  private looping = false;
  private loopDebounce = 1500;

  public queueMessage(message: Message) {
    this.messageQueue.push(message);
    this.startLoop();
  }

  private startLoop() {
    if (this.looping || this.messageQueue.length === 0) {
      return;
    }

    this.next();
  }

  private async next() {
    this.currentMessage$.next(null);

    if (this.messageQueue.length === 0) {
      this.looping = false;
      return;
    }

    if (this.looping) {
      await new Promise(resolve => setTimeout(resolve, this.loopDebounce));
    }

    this.looping = true;

    this.currentMessage$.next(this.messageQueue.shift());
    await new Promise(resolve => setTimeout(resolve, this.currentMessage$.value.ttl));

    this.next();
  }
}
