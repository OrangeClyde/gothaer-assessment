import { animate, style, transition, trigger } from '@angular/animations';

export const flyInOutTrigger = trigger('flyInOutAnimation', [
  transition(':enter', [
    style({ opacity: 0, transform: 'translateY(20%)' }),
    animate('300ms ease-out', style({ opacity: 1, transform: 'translateY(0%)' }))
  ]),
  transition(':leave', [
    style({ opacity: 1, transform: 'translateY(0%)' }),
    animate('300ms ease-in', style({ opacity: 0, transform: 'translateY(20%)' }))
  ])
]);
