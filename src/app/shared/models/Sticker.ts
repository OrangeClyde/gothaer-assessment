import { Price } from './Price';
import { Product } from './Product';

export class Sticker extends Product {
  public static fromObject(name: string, stickerObj: any): Sticker {
    const amount = parseInt(stickerObj.info.amount, 10);
    const price = new Price(stickerObj.info.currency, stickerObj.info.price);

    return new Sticker(name, stickerObj.availability, price, amount);
  }

  constructor(name: string, availability: string, public price: Price, public amount: number) {
    super(name, availability);
  }
}
