export enum MessageType {
  Info,
  Danger
}

export class Message {
  constructor(public text: string, public type: MessageType = MessageType.Info, public ttl: number = 3000) {}
}
