import { Price } from './Price';
import { Product } from './Product';

export class Insurance extends Product {
  public static fromObject(name: string, productObj: any): Insurance {
    const prices = productObj.prices.map(p => Price.fromObject(p));

    return new Insurance(name, productObj.availability, prices);
  }

  constructor(name: string, availability: string, public prices: Price[] = []) {
    super(name, availability);
  }

  public getPriceForAge(age: number): Price {
    return this.prices.find(p => p.minAge <= age && p.maxAge >= age);
  }
}
