export enum ProductType {
  Insurance = 'insurance',
  Sticker = 'sticker'
}

export class Product {
  get readableName(): string {
    return Product.convertSnakeCaseName(this.name);
  }

  public static convertSnakeCaseName(name: string = ''): string {
    return name.replace(/((?:^|_)[a-z])/g, (g: string) => g.toUpperCase().replace('_', ' '));
  }

  constructor(public name: string, public availability: string) {}
}
