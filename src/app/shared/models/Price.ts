export class Price {
  public static fromObject(priceObj: any) {
    const price = parseFloat(priceObj.price.replace(',', '.'));

    return new Price(priceObj.currency, price, priceObj.minAge, priceObj.maxAge, priceObj.label);
  }

  public static normalizeCurrency(currency: string): string {
    switch (currency.toUpperCase()) {
      case '€':
      case 'EURO':
        return '€';

      default:
        console.warn('Unknown currency "%s" detected.', currency);
        return '';
    }
  }

  constructor(
    public currency: string,
    public price: number,
    public minAge?: number,
    public maxAge?: number,
    public label?: string
  ) {
    this.currency = Price.normalizeCurrency(this.currency);
  }
}
