export class Customer {
  public static fromObject(customerObj: any): Customer {
    return new Customer(
      customerObj.name,
      customerObj.membership_type,
      parseInt(customerObj.age, 10),
      customerObj.selected_insurances,
      customerObj.password
    );
  }

  constructor(
    public name: string,
    public membership: string,
    public age: number,
    public selectedInsurances: string[],
    public password?: string
  ) {}
}
