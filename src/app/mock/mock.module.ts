import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { AuthenticationInterceptor } from '../authentication/interceptors/authentication-interceptor';
import { AuthenticationService } from '../authentication/services/authentication.service';
import { MockRequestInterceptor } from './interceptors/mock-request.interceptor';

@NgModule({
  declarations: [],
  imports: [],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: MockRequestInterceptor, multi: true }]
})
export class MockModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MockModule,
      providers: [
        AuthenticationService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthenticationInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: MockRequestInterceptor,
          multi: true
        }
      ]
    };
  }
}
