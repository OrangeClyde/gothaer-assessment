import { HttpErrorResponse, HttpEvent, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { delay } from 'rxjs/operators';

import { AuthenticationMock } from '../handlers/authentication.mock';
import { CustomerMock } from '../handlers/customer.mock.js';
import { MembershipMock } from '../handlers/membership.mock';
import { ProductMock } from '../handlers/product.mock.js';

@Injectable()
export class MockRequestInterceptor implements HttpInterceptor {
  private static readonly MOCK_DELAY = 300;

  private membershipMock: MembershipMock;
  private customerMock: CustomerMock;
  private authenticationMock: AuthenticationMock;
  private productMock: ProductMock;

  constructor() {
    console.warn(
      'Running with mocked API. All requests intercepted with a delay of %sms!',
      MockRequestInterceptor.MOCK_DELAY
    );

    this.membershipMock = new MembershipMock();
    this.productMock = new ProductMock();
    this.customerMock = new CustomerMock();
    this.authenticationMock = new AuthenticationMock(this.customerMock);
  }

  intercept(req: HttpRequest<any>): Observable<HttpEvent<any>> {
    let mockResponse: HttpEvent<any>;

    console.debug('Mocking %s %s', req.method, req.url);

    switch (`${req.method} ${req.url}`) {
      case 'POST /api/auth':
        mockResponse = this.authenticationMock.handlePostAuth(req);
        break;
      case 'POST /api/register':
        mockResponse = this.authenticationMock.handlePostRegister(req);
        break;
      case 'GET /api/products':
        mockResponse = this.productMock.handleGetProducts(req);
        break;
      case 'GET /api/memberships':
        mockResponse = this.membershipMock.handleGetMemberships(req);
        break;
    }

    if (req.method === 'GET' && /^\/api\/customers\//.test(req.url)) {
      mockResponse = this.customerMock.handleGetCustomer(req);
    }

    if (mockResponse instanceof HttpResponse) {
      console.debug('Response for %s: %o', req.url, mockResponse);

      return new Observable(subscriber => {
        subscriber.next(mockResponse);
        subscriber.complete();
      }).pipe(delay(MockRequestInterceptor.MOCK_DELAY)) as Observable<HttpEvent<any>>;
    }

    if (mockResponse instanceof HttpErrorResponse) {
      console.debug('Error response for %s: %o', req.url, mockResponse);

      return throwError(mockResponse).pipe(delay(MockRequestInterceptor.MOCK_DELAY)) as Observable<HttpEvent<any>>;
    }

    return this.handleMissingRoute(req);
  }

  private handleMissingRoute(req: HttpRequest<any>) {
    console.debug('Missing route for %s', req.url);

    const mockResponse = new HttpErrorResponse({
      url: req.url,
      status: 404,
      error: {}
    }) as any;

    return throwError(mockResponse);
  }
}
