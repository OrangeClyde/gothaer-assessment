import { HttpRequest, HttpResponse } from '@angular/common/http';

import * as productData from '../../../assets/data/products.json';

export class ProductMock {
  constructor() {}

  public handleGetProducts(req: HttpRequest<any>): HttpResponse<any> {
    const products = (productData as any).default;

    return new HttpResponse({
      url: req.url,
      body: products,
      status: 200
    });
  }
}
