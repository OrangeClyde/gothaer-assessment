import { HttpErrorResponse, HttpRequest, HttpResponse } from '@angular/common/http';

import * as customerData from '../../../assets/data/customers.json';

export class CustomerMock {
  constructor() {}

  public handleGetCustomer(req: HttpRequest<any>): HttpResponse<any> {
    const name = req.url.substr(req.url.lastIndexOf('/') + 1);
    const authHeader = req.headers.get('Authorization');

    if (!authHeader) {
      return new HttpErrorResponse({
        url: req.url,
        status: 401,
        error: 'Missing authentication header.',
        statusText: 'Unauthenticated'
      }) as any;
    }

    if (!this.isValidAuthHeader(authHeader, name)) {
      return new HttpErrorResponse({
        url: req.url,
        status: 403,
        error: `You can't access this ressource.`,
        statusText: 'Forbidden'
      }) as any;
    }

    const customer = this.getCustomerByName(name);

    if (!customer) {
      return new HttpErrorResponse({
        url: req.url,
        status: 404,
        error: `Customer not found.`,
        statusText: 'Not found'
      }) as any;
    }

    const strippedCustomer = {
      ...customer
    };
    delete strippedCustomer.password;

    return new HttpResponse({
      status: 200,
      url: req.url,
      body: strippedCustomer
    });
  }

  public getCustomerByName(name: string): any {
    const result = customerData.customers.filter(c => c.name === name);
    return result.length === 0 ? null : result[0];
  }

  public addNewCustomer(name: string, password: string, birthday: string): void {
    const birthdayDate = new Date(birthday);
    const ageDate = new Date(Date.now() - birthdayDate.getTime());
    const age = Math.abs(ageDate.getFullYear() - 1970);

    const rawCustomer = {
      name,
      membership_type: 'basic',
      password,
      age: `${age}`,
      selected_insurances: []
    };

    customerData.customers.push(rawCustomer);
  }

  private isValidAuthHeader(authHeader: string, name: string): boolean {
    return authHeader === `Bearer ${name}_fakeauthtoken`;
  }
}
