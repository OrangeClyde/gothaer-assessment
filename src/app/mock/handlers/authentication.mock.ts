import { HttpErrorResponse, HttpRequest, HttpResponse } from '@angular/common/http';

import { CustomerMock } from './customer.mock';

export class AuthenticationMock {
  constructor(private customerMock: CustomerMock) {}

  public handlePostAuth(req: HttpRequest<any>): HttpResponse<any> {
    const username = req.body.username;
    const password = req.body.password;

    const customer = this.customerMock.getCustomerByName(username);
    if (!customer || customer.password !== password) {
      return new HttpErrorResponse({
        url: req.url,
        status: 400,
        error: `Combination of username and password not found.`,
        statusText: 'Invalid credentials'
      }) as any;
    }

    return new HttpResponse({
      status: 200,
      body: {
        token: `${req.body.username}_fakeauthtoken`,
        ...customer
      }
    });
  }

  public handlePostRegister(req: HttpRequest<any>): HttpResponse<any> {
    const customer = this.customerMock.getCustomerByName(req.body.username);
    if (customer) {
      return new HttpErrorResponse({
        url: req.url,
        status: 400,
        error: `Username is already taken.`,
        statusText: 'Invalid username'
      }) as any;
    }

    this.customerMock.addNewCustomer(req.body.username, req.body.password, req.body.birthday);

    return new HttpResponse({
      status: 200
    });
  }
}
