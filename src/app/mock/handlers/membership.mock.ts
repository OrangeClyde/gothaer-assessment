import { HttpRequest, HttpResponse } from '@angular/common/http';

import * as membershipData from '../../../assets/data/memberships.json';

export class MembershipMock {
  constructor() {}

  public handleGetMemberships(req: HttpRequest<any>) {
    return new HttpResponse({
      status: 200,
      body: (membershipData as any).default,
      url: req.url
    });
  }
}
