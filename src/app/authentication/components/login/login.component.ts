import { HttpErrorResponse } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Message } from 'src/app/shared/models/Message';

import { flyInOutTrigger } from '../../../shared/animations/fly-in-out.animation';
import { MessageType } from '../../../shared/models/Message';
import { AuthenticationService } from '../../services/authentication.service';
import { MessageService } from './../../../shared/services/message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [flyInOutTrigger]
})
export class LoginComponent {
  @ViewChild('loginForm', { static: false })
  ngForm: NgForm;

  public username: string;
  public password: string;
  public loading: boolean;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private messageService: MessageService
  ) {}

  public signIn() {
    this.ngForm.form.disable();
    this.loading = true;

    this.authenticationService
      .authenticate(this.username, this.password)
      .pipe(first())
      .subscribe({
        next: this.onAuthenticationSuccess.bind(this),
        error: this.onAuthenticationFailure.bind(this)
      });
  }

  private onAuthenticationSuccess() {
    this.router.navigateByUrl('/');
  }

  private onAuthenticationFailure(err: HttpErrorResponse) {
    this.ngForm.form.enable();
    this.loading = false;
    this.messageService.queueMessage(
      new Message(err ? err.error : 'Something went wrong. Please try again.', MessageType.Danger)
    );
  }
}
