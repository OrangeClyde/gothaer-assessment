import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject, throwError } from 'rxjs';

import { SharedModule } from '../../../shared/shared.module';
import { AuthenticationModule } from '../../authentication.module';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let fixture: ComponentFixture<LoginComponent>;
  let component: LoginComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedModule,
        RouterTestingModule.withRoutes([]),
        HttpClientModule,
        AuthenticationModule.forRoot()
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should sign in on submit', () => {
    const authenticationSubject = new Subject<void>();

    spyOn(component.ngForm.form, 'disable');
    spyOn(component['authenticationService'], 'authenticate').and.returnValue(authenticationSubject);
    spyOn(component['router'], 'navigateByUrl');

    component.signIn();

    expect(component.loading).toBeTruthy();
    expect(component.ngForm.form.disable).toHaveBeenCalled();

    authenticationSubject.next();

    expect(component['router'].navigateByUrl).toHaveBeenCalledWith('/');
  });

  it('should handle an authentication error and populate a message', () => {
    spyOn(component.ngForm.form, 'enable');
    spyOn(component['authenticationService'], 'authenticate').and.returnValue(throwError({}));
    spyOn(component['messageService'], 'queueMessage');

    component.signIn();

    expect(component.ngForm.form.enable).toHaveBeenCalled();
    expect(component['messageService'].queueMessage).toHaveBeenCalled();
    expect(component.loading).toBeFalsy();
  });
});
