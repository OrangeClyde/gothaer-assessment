import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject, throwError } from 'rxjs';

import { SharedModule } from '../../../shared/shared.module';
import { RegistrationComponent } from './registration.component';

describe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), HttpClientModule, SharedModule],
      declarations: [RegistrationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should register on submit and sign in subsequently', () => {
    const registrationSubject = new Subject<void>();
    const authenticationSubject = new Subject<void>();

    spyOn(component.ngForm.form, 'disable').and.callThrough();
    spyOn(component['authenticationService'], 'register').and.returnValue(registrationSubject);
    spyOn(component['authenticationService'], 'authenticate').and.returnValue(authenticationSubject);
    spyOn(component['router'], 'navigateByUrl');

    component.register();

    expect(component.loading).toBeTruthy();
    expect(component.ngForm.form.disable).toHaveBeenCalled();

    registrationSubject.next();

    expect(component.loading).toBeTruthy();
    expect(component.ngForm.disabled).toBeTruthy();

    authenticationSubject.next();

    expect(component['router'].navigateByUrl).toHaveBeenCalledWith('/');
  });

  it('should handle a registration error and populate a message', () => {
    spyOn(component.ngForm.form, 'enable');
    spyOn(component['authenticationService'], 'register').and.returnValue(throwError({}));
    spyOn(component['messageService'], 'queueMessage');

    component.register();

    expect(component.ngForm.form.enable).toHaveBeenCalled();
    expect(component['messageService'].queueMessage).toHaveBeenCalled();
    expect(component.loading).toBeFalsy();
  });
});
