import { HttpErrorResponse } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Message } from 'src/app/shared/models/Message';

import { MessageType } from '../../../shared/models/Message';
import { AuthenticationService } from '../../services/authentication.service';
import { MessageService } from './../../../shared/services/message.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  public username: string;
  public password: string;
  public passwordConfirmation: string;
  public birthday: Date;

  public loading: boolean;

  @ViewChild('registrationForm', { static: false })
  public ngForm: NgForm;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private messageService: MessageService
  ) {}

  public register() {
    this.loading = true;
    this.ngForm.form.disable();

    this.authenticationService
      .register(this.username, this.password, this.birthday)
      .pipe(first())
      .subscribe({
        next: this.onRegistrationSuccess.bind(this),
        error: this.onRegistrationFailure.bind(this)
      });
  }

  private onRegistrationSuccess() {
    this.authenticationService
      .authenticate(this.username, this.password)
      .pipe(first())
      .subscribe({
        next: this.onAuthenticationSuccess.bind(this),
        error: this.onRegistrationFailure.bind(this)
      });
  }

  private onAuthenticationSuccess() {
    this.router.navigateByUrl('/');
  }

  private onRegistrationFailure(err: HttpErrorResponse) {
    this.ngForm.form.enable();
    this.loading = false;
    this.messageService.queueMessage(
      new Message(err ? err.error : 'Something went wrong. Please try again.', MessageType.Danger)
    );
  }
}
