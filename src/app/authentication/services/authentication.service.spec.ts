import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MockModule } from '../../mock/mock.module';
import { AuthenticationModule } from '../authentication.module';
import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        HttpClientModule,
        AuthenticationModule.forRoot(),
        MockModule.forRoot()
      ]
    });
    service = TestBed.get(AuthenticationService);

    localStorage.removeItem('goth__auth__token');
    localStorage.removeItem('goth__auth__customerName');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be initialized without stored auth', () => {
    service.init();

    expect(service.authenticationState$.value).toBeFalsy();
  });

  it('should be initialized with stored auth', () => {
    localStorage.setItem('goth__auth__token', 'foo');
    localStorage.setItem('goth__auth__customerName', 'bar');

    service.init();

    expect(service.authenticationState$.value).toBeTruthy();
  });

  it('should authenticate', async () => {
    await service.authenticate('person1', 'password1').toPromise();

    expect(service.token).toBeTruthy();
    expect(service.customerName).toBeTruthy();
    expect(service.authenticationState$.value).toBeTruthy();
  });

  it('should register', async () => {
    await service.register('new', 'new', new Date()).toPromise();
    await service.authenticate('new', 'new').toPromise();

    expect(service.token).toBeTruthy();
    expect(service.customerName).toBeTruthy();
    expect(service.authenticationState$.value).toBeTruthy();
  });

  it('should sign out and redirect to login', () => {
    service.token = 'foo';
    service.customerName = 'bar';
    spyOn(service['router'], 'navigateByUrl');

    service.signOut();

    expect(service.token).toBeFalsy();
    expect(service.customerName).toBeFalsy();
    expect(service.authenticationState$.value).toBeFalsy();
    expect(service['router'].navigateByUrl).toHaveBeenCalledWith('/login');
  });
});
