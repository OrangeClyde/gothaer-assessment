import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public token = '';
  public customerName = '';

  public authenticationState$ = new BehaviorSubject<boolean>(false);

  constructor(private router: Router, private httpClient: HttpClient) {}

  public init() {
    try {
      this.token = localStorage.getItem('goth__auth__token');
      this.customerName = localStorage.getItem('goth__auth__customerName');

      this.authenticationState$.next(!!this.token && !!this.customerName);
    } catch {
      // No authentication data available in local storage.
      // Proceeding unauthenticated.
      this.authenticationState$.next(false);
    }
  }

  public authenticate(username: string, password: string): Observable<any> {
    return this.httpClient.post('/api/auth', { username, password }).pipe(map(this.onAuthenticationSuccess.bind(this)));
  }

  public register(username: string, password: string, birthday: Date): Observable<any> {
    return this.httpClient.post('/api/register', { username, password, birthday });
  }

  public signOut() {
    this.token = '';
    this.customerName = '';
    this.storeData();

    this.authenticationState$.next(false);

    this.router.navigateByUrl('/login');
  }

  private onAuthenticationSuccess(data: { token: string; name: string }) {
    this.token = data.token;
    this.customerName = data.name;
    this.storeData();

    this.authenticationState$.next(true);

    return data;
  }

  private storeData() {
    localStorage.setItem('goth__auth__token', this.token);
    localStorage.setItem('goth__auth__customerName', this.customerName);
  }
}
