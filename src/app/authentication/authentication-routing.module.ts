import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { AuthenticationRequiredGuard } from './guards/authentication-required.guard';

const routes: Route[] = [
  {
    path: 'register',
    component: RegistrationComponent,
    canActivate: [AuthenticationRequiredGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthenticationRequiredGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule {}
