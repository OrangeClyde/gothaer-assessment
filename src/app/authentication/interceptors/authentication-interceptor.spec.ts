import { HttpClientModule, HttpRequest } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthenticationService } from '../services/authentication.service';
import { AuthenticationInterceptor } from './authentication-interceptor';

describe('AuthenticationInterceptor', () => {
  let interceptor: AuthenticationInterceptor;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule.withRoutes([])],
      providers: [AuthenticationInterceptor, AuthenticationService]
    }).compileComponents();
  }));

  beforeEach(() => {
    interceptor = TestBed.get(AuthenticationInterceptor);
  });

  it('should create', () => {
    expect(interceptor).toBeTruthy();
  });

  it('should inject an authentication token', () => {
    interceptor['authenticationService'].token = 'foo';

    const httpHandlerStub = {
      handle: jasmine.createSpy('handle')
    };
    interceptor.intercept(new HttpRequest<any>('GET', 'foo'), httpHandlerStub);

    const authReq: HttpRequest<any> = httpHandlerStub.handle.calls.first().args[0];
    expect(authReq.headers.get('Authorization')).toEqual('Bearer foo');
  });
});
