import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthenticationGrantedGuard } from './authentication/guards/authentication-granted.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule), // 'profile/profile.module#ProfileModule',
    canActivate: [AuthenticationGrantedGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
