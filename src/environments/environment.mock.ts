import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { MockRequestInterceptor } from '../app/mock/interceptors/mock-request.interceptor';

export const environment = {
  production: false,
  mock: true,
  interceptors: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MockRequestInterceptor,
      multi: true
    }
  ]
};
